# Recipe App API Proxy

NGINX proxy for the Recipe Django App

## Usage

**Environment variables**

|Env Var|Default|Purpose|
|---|---|---|
|LISTEN_PORT|8000|Port the NGINX server listens on|
|APP_HOST|app|Name of the Django app (Docker container name)|
|APP_PORT|9000|Port Django runs on|


