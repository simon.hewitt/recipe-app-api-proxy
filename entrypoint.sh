#!/bin/sh
# Entrypoint for NGINX Docker service

echo "Starting NGINX"
set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

echo "Running NGINX now:"
nginx -g "daemon off;"
